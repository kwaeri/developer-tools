# [![Patreon](https://img.shields.io/badge/Patreon-Funding-inactive?style=for-the-badge&logo=patreon&color=FF424D)](https://patreon.com/kirvedx) kwaeri-developer-tools [![PayPal](https://img.shields.io/badge/PayPal-Donations-inactive?style=for-the-badge&logo=paypal&color=253B80)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)

A Massively Modified Open Source Project by kirvedx

[![GPG/Keybase](https://img.shields.io/badge/GPG-1B842CB5%20Rik-inactive?style=for-the-badge&label=GnuPG2%2FKeybase&logo=gnu+privacy+guard&color=0093dd)](https://keybase.io/rik)
[![Google](https://img.shields.io/badge/Google%20Developers-kirvedx-inactive?style=for-the-badge&logo=google+tag+manager&color=414141)](https://developers.google.com/profile/u/117028112450485835638)
[![GitLab](https://img.shields.io/badge/GitLab-kirvedx-inactive?style=for-the-badge&logo=gitlab&color=fca121)](https://github.com/kirvedx)
[![GitHub](https://img.shields.io/badge/GitHub-kirvedx-inactive?style=for-the-badge&logo=github&color=181717)](https://github.com/kirvedx)
[![npm](https://img.shields.io/badge/NPM-Rik-inactive?style=for-the-badge&logo=npm&color=CB3837)](https://npmjs.com/~rik)

kwaeri-developer-tools (kdt) is a minimal set of tools for aiding rapid development of application logic. It is also a dependency of several components which make up the @kwaeri/platform.

[![pipeline status](https://gitlab.com/kwaeri/developer-tools/badges/main/pipeline.svg)](https://gitlab.com/kwaeri/developer-tools/commits/main)  [![coverage report](https://gitlab.com/kwaeri/developer-tools/badges/main/coverage.svg)](https://kwaeri.gitlab.io/developer-tools/coverage/)  [![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/1879/badge)](https://bestpractices.coreinfrastructure.org/projects/1879)

> WARNING! kdt is not ready for production yet. We have decided to publish early for testing purposes during development. You are free to check us out, but please note that this project is by no means complete, nor safe, and we **DO NOT** recommend using kwdt at this time. With that said, please feel free to check the library out and see where it's headed!

## Table of Contents

* [The Implementation (about kdt)](#the-implementation)
  * [Developer Hype](#developers)
* [Using kdt](#using-kdt)
  * [Installation](#installation)
  * [Integration](#integration)
    * [JavaScript](#javascript)
* [How to Contribute Code](#how-to-contribute-code)
* [Other Ways to Contribute](#other-ways-to-contribute)
  * [Bug Reports](#bug-reports)
  * [Vulnerability Reports](#vulnerability-reports)
    * [Confidential Issues](#confidential-issues)
  * [Donations](#donations)

## The Implementation

The library provides potentially familiar methods; `.type()`, `.isNumber()`, `isVisible()`, `.empty()`, `.extend()`, `.each()`, `.has()`, `.get()`, `.set()`, and recently added - a mixin composer, similarly named `.compose()`. We may very well add more tools to the collection as we continue to grow the project, though we've found that these tools more than suffice in helping us to rapidly develop logic on the server-side of any Node.js application.

Browse [documentation](https://kwaeri.gitlab.io/developer-tools/) for more information, including API reference (*still in development*).

If you're looking for a similar type of tooling for the client/browser - take a peek at our [web development tools](https://mmod.gitlab.io/kwaeri-web-developer-tools).

### Developers

This project makes use of TypeScript, and recently rid itself of babel, gulp, and istanbul in favor of a direct typescript, mocha, nyc approach that is implemented as a pure ESM module. If you require CommonJS stick with version 0.3.0; otherwise, track 0.4.0+ from here on out.

## Using kdt

To use kdt in your own project/application, install the module via npm and include it in your application:.

### Installation

You can obtain kdt a couple of different ways:

* Via npm with `npm install @kwaeri/developer-tools` in a terminal/command prompt.
* Via direct download in several available formats:
  * [.zip](https://gitlab.com/kwaeri/developer-tools/-/archive/main/developer-tools-master.zip)
  * [.tar.gz](https://gitlab.com/kwaeri/developer-tools/-/archive/main/developer-tools-master.tar.gz)
  * [.tar.bz2](https://gitlab.com/kwaeri/developer-tools/-/archive/main/developer-tools-master.tar.bz2)
  * [.tar](https://gitlab.com/kwaeri/developer-tools/-/archive/main/developer-tools-master.tar)

With either option, after obtaining the source (and building the project with the latter option), the production files can be found within the `dist` subdirectory.

### Integration

Include the module anywhere you wish to make use of kdt:

```javascript
import { kdt } from '@kwaeri/developer-tools';
```

Source maps are provided, so feel free to use kdt in development.

Once you have loaded the module into your application, you must initialize it:

```javascript
const _ = new kdt();
```

You're now set to make use of kdt in your project.

## How to Contribute Code

Our Open Source projects are always open to contribution. If you'd like to cocntribute, all we ask is that you follow the guidelines for contributions, which can be found at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Contribute-Code)

There you'll find topics such as the guidelines for contributions; step-by-step walk-throughs for getting set up, [Coding Standards](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Coding-Standards), [CSS Naming Conventions](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/CSS-Naming-Conventions), and more.

The project also leverages Keybase for communication and alerts - outside of standard email. To join our keybase chat, run the following from terminal (assuming you have [keybase](https://www.keybase.io) installed and running):

```bash
keybase team request-access kwaeri
```

Alternatively, you could search for the team in the GUI application and request access from there.

## Other Ways to Contribute

There are other ways to contribute to the project other than with code. Consider [testing](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Test-Code) the software, or in case you've found a [Bug](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports) - please report it. You can also support the project monetarily through [donations](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Donations) via PayPal.

Regardless of how you'd like to contribute, you can also find in-depth information for how to do so at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute)

### Bug Reports

To submit bug reports, request enhancements, and/or new features - please make use of the **issues** system baked-in to our source control project space at [GitLab](https://gitlab.com/mmod/kwaeri-web-developer-tools/issues)

You may optionally start an issue, track, and manage it via email by sending an email to our project's [support desk](mailto:contact-project+kwaeri-developer-tools-8511661-issue-@incoming.gitlab.com).

For more in-depth documentation on the process of submitting bug reports, please visit the [Massively Modified Wiki on Bug Reports](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports)

### Vulnerability Reports

Our Vulnerability Reporting process is very similar to Gitlab's. In fact, you could say its a *fork*.

To submit vulnerability reports, please email our [Security Group](mailto:security@mmod.co). We will try to acknowledge receipt of said vulnerability by the next business day, and to also provide regular updates about our progress. If you are curious about the status of your report feel free to email us again. If you wish to encrypt your disclosure email, like with gitlab - please email us to ask for our GPG Key.

Please refrain from requesting compensation for reporting vulnerabilities. We will publicly acknowledge your responsible disclosure, if you request us to do so. We will also try to make the confidential issue public after the vulnerability is announced.

You are not allowed, and will not be able, to search for vulnerabilities on Gitlab.com. As our software is open source, you may download a copy of the source and test against that.

#### Confidential Issues

When a vulnerability is discovered, we create a [confidential issue] to track it internally. Security patches will be pushed to private branches and eventually merged into a `security` branch. Security issues that are not vulnerabilites can be seen on our [public issue tracker](https://gitlab.com/mmod/kwaeri-web-developer-tools/issues?label_name%5B%5D=Security).

For more in-depth information regarding vulnerability reports, confidentiality, and our practices; Please visit the [Massively Modified Wiki on Vulnerability](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Vulnerability-Reports)

### Donations

If you cannot contribute time or energy to neither the code base, documentation, nor community support; please consider making a monetary contribution which is extremely useful for maintaining the Massively Modified network and all the goodies offered free to the public.

[![Donate via PayPal.com](https://gitlab.com/mmod/kwaeri-user-experience/raw/master/images/mmod-donate-btn-2.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)

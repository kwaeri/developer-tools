/**
 * SPDX-PackageName: kwaeri/developer-tools
 * SPDX-PackageVersion: 0.10.1
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


 'use strict'


 // INCLUDES

 // ESM WRAPPER
 export {
    kdt
} from './src/kdt.mjs';

